//soal 1
console.log("----soal 1----")


const luasLingkaran = (r)=>{
  const pi = r % 7 === 0 ? (22/7) : 3.14
  let luas = pi * r * r
  return luas
}

const kelilingLingkaran = (r) =>{
  const pi = r % 7 === 0 ? (22/7) : 3.14
  let keliling = 2 * pi * r
  return keliling
}

console.log(luasLingkaran(7))
console.log(kelilingLingkaran(7))

console.log()

//soal 2
console.log("----soal 2----")

let kalimat =""

const tambahkanKata = (str) =>{
  kalimat = `${kalimat} ${str}`
}

tambahkanKata("saya")
tambahkanKata("adalah")
tambahkanKata("seorang")
tambahkanKata("frontend")
tambahkanKata("developer")

console.log(kalimat)

console.log()

// Soal 3
console.log("----Soal 3----")

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`)
    }
  }
}

newFunction("William", "Imoh").fullName()

console.log()

// Soal 4
console.log("----Soal 4----")

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}


const { firstName, lastName, destination, occupation, spell } = newObject

console.log(firstName, lastName, destination, occupation, spell)
console.log()



// Soal 5
console.log("----Soal 5----")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]


console.log(combined)
